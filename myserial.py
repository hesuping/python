﻿#! /usr/bin/env python
# -*- coding:utf-8 -*-

import threading
import time
import serial 
import serial.tools.list_ports 
	
class ComThread():
	def __init__(self):
		self.l_serial = None
		self.alive = False
		self.waitEnd = None
		self.port = 'COM1'
		self.data = None

	def waiting(self):
		if not self.waitEnd is None:
			self.waitEnd.wait()

	def SetStopEvent(self):
		if not self.waitEnd is None:
			self.waitEnd.set()
		self.alive = False
		self.stop()

	def start(self):
		self.l_serial = serial.Serial()
		self.l_serial.port = self.port
		self.l_serial.baudrate = 9600
		self.l_serial.bytesize = 8
		self.l_serial.stopbits = 1
		self.l_serial.parity = serial.PARITY_EVEN
		self.l_serial.timeout = 2
		self.l_serial.open()
		if self.l_serial.isOpen():
			self.waitEnd = threading.Event()
			self.alive = True
			self.thread_read = None
			self.thread_read = threading.Thread(target=self.UartRead)
			self.thread_read.setDaemon(1)
			self.thread_read.start()
			return True
		else:
			return False

	def SendDate(self,send):
		isOK = False
		try:
			self.l_serial.write(send)
		except Exception as ex:
			pass;
		return isOK

	def UartRead(self):
		while self.alive:
			time.sleep(0.1)

			data = ''
			data = data.encode('utf-8')

			n = self.l_serial.inWaiting()
			if n:
				data = data + self.l_serial.read(n)
				print('get data from serial port:', data.decode().strip())

	def stop(self):
		self.alive = False
		self.thread_read.join()
		if self.l_serial.isOpen():
			self.l_serial.close()


def main():
	port_list = list(serial.tools.list_ports.comports()) 
	if len(port_list) <= 0: 
		print("No available COM!" )
	else:
		temp_serial = list() 
		for port_com in port_list:
			port_list_0 =list(port_com) 
			port_serial = port_list_0[0] 
			ser = serial.Serial(port_serial,9600,timeout = 2)
			temp_serial.append(ser.name)
		print("Currently available COM is: ",temp_serial)
	uart = ComThread()
	uart.port = input("select the COM you want:")
	uart.start()
	uart.waiting()
#	uart.SendDate('11111'.encode())
#	uart.stop()
#	while 1:
#		time.sleep(1)

if __name__ == '__main__':
	main()





